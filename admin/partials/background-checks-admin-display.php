<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://everydayshuffle.com
 * @since      1.0.0
 *
 * @package    Background_Checks
 * @subpackage Background_Checks/admin/partials
 */

global $wpdb;
$table_name = $wpdb->prefix . 'bc_status';

$results = $wpdb->get_results("SELECT status.time, status.order_id, status.percentage_complete, status.status, status.candidate_id,
								CONCAT(
									(SELECT meta_value FROM wp_usermeta WHERE user_id = users.id AND meta_key = 'first_name'),
									' ',
									(SELECT meta_value FROM wp_usermeta WHERE user_id = users.id AND meta_key = 'last_name')
								) as name , users.user_email as email 
								FROM (wp_bc_status as status , wp_users as users ) 
								WHERE status.user_id =  users.id");

?>

<div class="wrap">
	 <?php settings_errors(); ?>

	 <?php
		$active_tab = isset($_GET[ 'tab' ]) ? $_GET[ 'tab' ] : 'api_credentials';
	 ?>

	 <h2 class="nav-tab-wrapper">
   		<a href="?page=admin-background-checks&tab=api_credentials" class="nav-tab">API credentials</a>
    	<a href="?page=admin-background-checks&tab=requests_status" class="nav-tab">BC request status</a>
	 </h2>

	 <?php if($active_tab == 'api_credentials'){ ?>
			<div id="api_credentials">
				<h2>Accurate API credentials</h2>
			 	<form method="post" action="options.php">
			 	<?php settings_fields( 'bc-plugin-settings-group' ); ?>
    			<?php do_settings_sections( 'bc-plugin-settings-group' ); ?>
				   <table class="form-table">
				      <tr valign="top">
				      <th scope="row">Account ID</th>
				      <td>
				      	<input type="text" class="admin-input" name="bc_accurate_id" value="<?php echo esc_attr( get_option('bc_accurate_id') ); ?> "/>
				      </td>
				      </tr>
				      <tr valign="top">
				      <th scope="row">Account secret</th>
				      <td>
				      	<input type="text" class="admin-input" name="bc_accurate_secret" value="<?php echo esc_attr( get_option('bc_accurate_secret') ); ?>"/>
				      </td>
				      </tr>
				    </table>
				    <h3>Select Background checks order page.</h3>
				    <p>Page where you will paste [bc_purchase] shortcode.</p>
				    <?php 
				    	$args = array(
							'selected' => get_option('bc_accurate_order_page'),
							'name'             => 'bc_accurate_order_page'
						);
				    	wp_dropdown_pages($args); 
				    ?>
				    <?php submit_button(); ?>
		  		</form>
	  		</div>
	  	<?php }else if($active_tab == 'requests_status'){ ?>
	  		 <div id="requests_status">
	  		   <?php 
				  // The Loop
				  if ( $results ) {
					echo '<table class="widefat fixed" cellspacing="0">
					 		<thead>
							<tr valign="top">
					      		<th scope="row">User Name</th>
					      		<th scope="row">User Email</th>
					      		<th scope="row">Order ID</th>
					      		<th scope="row">Candidate ID</th>
					      		<th scope="row">Request date</th>
					      		<th scope="row">Percentage Complete</th>
					      		<th scope="row">Status</th>
					      	</tr>
					      	</thead>';
							foreach ($results as $result) {
								echo '<tr class="alternate">';
								echo '<td>' . $result->name .'</td>';
								echo '<td>' . $result->email . '</td>';
								echo '<td>' . $result->order_id . '</td>';
								echo '<td>' . $result->candidate_id . '</td>';
								echo '<td>' . $result->time . '</td>';
								echo '<td>' . $result->percentage_complete . ' %</td>';
								echo '<td>' . $result->status . '</td>';
								echo '</tr>';
							}
					echo '</table>';
							/* Restore original Post Data */
							wp_reset_postdata();
						} else {
							echo '<h3>No items found</h3>';
						}				    
				?>
		  	 </div>
	  	<?php }?>
	  	
</div>
