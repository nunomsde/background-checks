<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://everydayshuffle.com
 * @since             1.0.0
 * @package           Background_Checks
 *
 * @wordpress-plugin
 * Plugin Name:       Background Checks
 * Plugin URI:        http://everydayshuffle.com
 * Description:       Plugin to execute background checks on users using Accurate background API
 * Version:           1.0.0
 * Author:            Nuno Duarte
 * Author URI:        http://everydayshuffle.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       background-checks
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// db versioning vars 
global $jal_db_version;
$jal_db_version = '1.0';

// require API 
require_once plugin_dir_path(__FILE__) . 'includes/class-accurate-api.php';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-background-checks-activator.php
 */
function activate_background_checks() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-background-checks-activator.php';
	Background_Checks_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-background-checks-deactivator.php
 */
function deactivate_background_checks() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-background-checks-deactivator.php';
	Background_Checks_Deactivator::deactivate();
}


/*
*
* Create database table to track background requests status
*
*/
function bc_create_status_table(){
	global $wpdb;
	global $jal_db_version;

	$table_name = $wpdb->prefix . 'bc_status';

	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		user_id mediumint(9) NOT NULL,
		order_id text NOT NULL,
		candidate_id text NOT NULL,
		percentage_complete varchar(55) DEFAULT '' NOT NULL,
		status varchar(55) DEFAULT '' NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'jal_db_version', $jal_db_version );
}

/*
*
* Register a woocommerce BC product 
*
*/ 
function bc_register_woocommerce_product(){
	//verify if woocommerce is active
	if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
   		
   		$args = array(
		    'post_author' => $user_id,
		    'post_content' => 'Produc that allows users to pay for their background checks.',
		    'post_status' => "publish",
		    'post_title' => "Background Check",
		    'post_parent' => '',
		    'post_type' => "product",
		);

		//Create product
		$post_id = wp_insert_post( $args, $wp_error );

		wp_set_object_terms( $post_id, 'Background Checks', 'product_cat' );
		wp_set_object_terms($post_id, 'simple', 'product_type');

		update_post_meta( $post_id, '_visibility', 'visible' );
		update_post_meta( $post_id, '_stock_status', 'instock');
		update_post_meta( $post_id, 'total_sales', '0');
		update_post_meta( $post_id, '_downloadable', 'no');
		update_post_meta( $post_id, '_virtual', 'yes');
		update_post_meta( $post_id, '_regular_price', "40" );
		update_post_meta( $post_id, '_purchase_note', "" );
		update_post_meta( $post_id, '_featured', "no" );
		update_post_meta( $post_id, '_weight', "" );
		update_post_meta( $post_id, '_length', "" );
		update_post_meta( $post_id, '_width', "" );
		update_post_meta( $post_id, '_height', "" );
		update_post_meta($post_id, '_sku', "");
		update_post_meta( $post_id, '_product_attributes', array());
		update_post_meta( $post_id, '_sale_price_dates_from', "" );
		update_post_meta( $post_id, '_sale_price_dates_to', "" );
		update_post_meta( $post_id, '_price', "40" );
		update_post_meta( $post_id, '_sold_individually', "yes" );
		update_post_meta( $post_id, '_manage_stock', "no" );
		update_post_meta( $post_id, '_backorders', "no" );
		update_post_meta( $post_id, '_stock', "" );
	}
}

/*
*
* Delete BC product when plugin is disabled
*
*/
function bc_unregister_woocommerce_product(){
	if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		
		$args = array( 'post_type' => 'product', 'posts_per_page' => 1, 'post_title' => 'Background Check' );
	    $loop = new WP_Query( $args );

	    foreach ($loop as $product) {
	    	// delete product with force delete
	    	wp_delete_post($product->ID,true);
	    }

	    wp_reset_query(); 
	}
}


register_activation_hook( __FILE__, 'activate_background_checks' );
register_deactivation_hook( __FILE__, 'deactivate_background_checks' );

register_activation_hook( __FILE__, 'bc_create_status_table' );
register_activation_hook(__FILE__,'bc_register_woocommerce_product');

register_deactivation_hook(__FILE__,'bc_unregister_woocommerce_product');


add_action('woocommerce_order_status_completed','bc_order_status_complete');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-background-checks.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_background_checks() {

	$plugin = new Background_Checks();
	$plugin->run();

}
run_background_checks();

/*
*
* Add admin menu 
*
*/
add_action( 'admin_menu', 'bc_admin_menu' );

function bc_admin_menu() {
	add_menu_page( 'BC Admin options', 'Background Checks', 'manage_options', 'admin-background-checks', 'bc_admin_page', 'dashicons-shield-alt', 6  );
}

/*
*
* Register plugin settings
*
*/
add_action( 'admin_init', 'bc_register_settings' );

function bc_register_settings() {
	//register our settings
	register_setting( 'bc-plugin-settings-group', 'bc_accurate_id' );
	register_setting( 'bc-plugin-settings-group', 'bc_accurate_secret' );
	register_setting( 'bc-plugin-settings-group', 'bc_accurate_order_page' );
}


function bc_admin_page(){
	include plugin_dir_path( __FILE__ ) . 'admin/partials/background-checks-admin-display.php';
}

/*
*
* Add shortcode to allow users to purchase a 
* BC via woocommerce from a specific page
*
*/
function bc_purchase_shortcode(){
	if(!bc_user_is_checked()){
		include plugin_dir_path(__FILE__) . 'public/partials/background-checks-purchase-button.php';
	}else{
		echo '<h3>You already requested a background check.</h3>';
	}
}
add_shortcode('bc_purchase', 'bc_purchase_shortcode');

/*
*
* Add shortcode to setup Webhook page for Accurate API. 
*
*/
function bc_webhook_shortcode(){
	if(isset($_POST)){
		$response = wp_remote_retrieve_body($_POST);
		bc_update_order_status($response);
	}
}
add_shortcode('bc_webhook' , 'bc_webhook_shortcode');

/*
*
*
* Shortcode to add background checks suggestion modal to pages.
*
*/
function bc_add_modal(){

		$page_id = get_option('bc_accurate_order_page');

		echo "<script type='text/javascript'>
				(function($) {
					if(!$.cookie('bc_modal')){
			    		swal({  title: 'What to be a certified caregiver!',   
			    				text: 'We run background checks to confirm your identity and credentials. It will help you to get more costumers. Only 40$.',   
			    				type: 'success',   
			    				allowEscapeKey: true,
			    				showCancelButton: true,   
			    				confirmButtonText: 'Yes, lets do it!',   
			    				closeOnConfirm: false 
			    			}, 
			    			function(isConfirm){   
			    				if (isConfirm) {
			    					$.cookie('bc_modal','wait',{ expires: 5 });
			    					window.location.href = '".get_page_link($page_id)."';
			    				}else{
			    					$.cookie('bc_modal','wait',{ expires: 10 });
								   swal.close();
			    				}
							});
						}
				}(jQuery));
	    	 </script>";
}
add_shortcode('bc_modal','bc_add_modal');

/*
*
* Add product to cart and reditect to cart page after order BC
*
*/
function bc_on_product_purchase(){
	if (isset($_POST['bc-purchase'])){
		$user_id = get_current_user_id();

 		$state = get_user_meta( $user_id, 'state', true );
  		$city = get_user_meta($user_id , 'city', true);
 	
 		if($state != "" || $city != ""){
	        if ($_POST['bc-purchase'] == "submitted") {
	          $product = bc_fetch_product();	
	         // if ( ! is_admin() ) {
				$found = false;
				//check if product already in cart
				if ( sizeof( WC()->cart->get_cart() ) > 0 ) {
					foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
						$_product = $values['data'];
						if ( $_product->id == $product->ID )
							$found = true;
					}
					// if product not found, add it
					if ( ! $found )
						WC()->cart->add_to_cart( $product->ID );
				} else {
					// if no products in cart, add it
					WC()->cart->add_to_cart( $product->ID );
				}
			//}

	          redirect_to_checkout();
	        }
	    }else{
	    	// we need to redirect user to the Account edit page
	    	//wp_redirect(WC()->account->get_account_url());
	    	//wc_customer_edit_account_url();
    		//exit;
	    }
    }
}
add_action('init', 'bc_on_product_purchase');

/*
*
* Redirect to checkout adter adding BC product
*
*/
function redirect_to_checkout() {
    wp_redirect(WC()->cart->get_checkout_url());
    exit;
}

/*
*
* Fetch BC product from database
*
*/
function bc_fetch_product(){
	return get_page_by_title('Background Check',OBJECT,'product') ;
}

/*
*
* Insert user request to bc_status table
* Function is called when order status is set to complete. 
* (Pyament process is finished)
*/
function bc_insert_to_status_table(){
	global $wpdb;
	$table_name = $wpdb->prefix . 'bc_status';
	$user = wp_get_current_user();
  	$state = get_user_meta( $user->ID, 'state', true );
  	$city = get_user_meta($user->ID, 'city', true);

	$account_id = get_option('bc_accurate_id');
	$account_secret = get_option('bc_accurate_secret');
	
	//Instance of Accurate_Api class
	$Accurate_API = new Accurate_API($account_id,$account_secret);

	//create candidate at Accurate system
	$candidate = $Accurate_API->createCandidate($user);

	// verify if the candiadate result is an error code
	if($candidate->errors)

	// create order for background check at Accurate system
	$order = $Accurate_API->createOrder($candidate->id, $state, $city);

	// verify if the order result is an error code
	if($order->errors)

	if($candidate->id && $order->id){
		$wpdb->insert( 
			$table_name, 
			array( 
				'time' => current_time( 'mysql' ), 
				'user_id' => $user->ID, 
				'order_id' => $order->id,
				'candidate_id' => $candidate->id,
				'percentage_complete' => $order->percentageComplete,
				'status' => $order->status
			) 
		);
	}
}

/*
*
* Called when order status changes to complete. 
* checks all order products looking for a BC product, 
* if it finds one, starts the BC process.
* 
*/ 
function bc_order_status_complete($order_id){
	$order = new WC_Order( $order_id );
	$items = $order->get_items(); 
	// check order items for BC item
	foreach ( $items as $item ) {
	   if ( $item['name'] == "Background Check" ) {
	   		bc_insert_to_status_table();
	   }
	}
}


/*
*
* Extend woocommerce account form to support user state and city
*
*/
add_action( 'woocommerce_edit_account_form', 'bc_woocommerce_edit_account_form' );
add_action( 'woocommerce_save_account_details', 'bc_woocommerce_save_account_details' );
 
function bc_woocommerce_edit_account_form() {
 
  $user_id = get_current_user_id();
  $user = get_userdata( $user_id );
 
  if ( !$user )
    return;
 
  $state = get_user_meta( $user_id, 'state', true );
  $city = get_user_meta($user_id , 'city', true);

  
  ?>
 
  <fieldset>
    <legend>Location information</legend>
    <p>Fill in this information about your location for background checks purposes.</p>
    <p class="form-row form-row-thirds">
      <label for="state">State:</label>
      <?php echo bc_states_dropdown($state); ?>
    </p>
     <p class="form-row form-row-thirds">
      <label for="city">City:</label>
      <input type="text" name="city" value="<?php echo esc_attr( $city ); ?>" class="input-text" />
    </p>
  </fieldset>
  <?php
 
}

function bc_states_dropdown($code){

	$states_list = array(
  					'AL' => 'Alabama',
  					'AK' => 'Alaska',
  					'AZ' => 'Arizona',
  					'AR' => 'Arkansas',
  					'CA' => 'California',
  					'CO' => 'Colorado',
  					'CT' => 'Connecticut',
  					'DE' => 'Delaware',
  					'DC' => 'District Of Columbia',
  					'FL' => 'Florida',
  					'GA' => 'Georgia',
  					'HI' => 'Hawaii',
  					'ID' => 'Idaho',
  					'IL' => 'Illinois',
  					'IN' => 'Indiana',
  					'IA' => 'Iowa',
  					'KS' => 'Kansas',
  					'KY' => 'Kentucky',
  					'LA' => 'Louisiana',
  					'ME' => 'Maine',
  					'MD' => 'Maryland',
  					'MA' => 'Massachusetts',
  					'MI' => 'Michigan',
  					'MN' => 'Minnesota',
  					'MS' => 'Mississippi',
  					'MO' => 'Missouri',
  					'MT' => 'Montana',
  					'NE' => 'Nebraska',
  					'NV' => 'Nevada',
  					'NH' => 'New Hampshire',
  					'NJ' => 'New Jersey',
  					'NM' => 'New Mexico',
  					'NY' => 'New York',
  					'NC' => 'North Carolina',
  					'ND' => 'North Dakota',
  					'OH' => 'Ohio',
  					'OK' => 'Oklahoma',
  					'OR' => 'Oregon',
  					'PA' => 'Pennsylvania',
  					'RI' => 'Rhode Island',
  					'SC' => 'South Carolina',
  					'SD' => 'South Dakota',
  					'TN' => 'Tennessee',
  					'TX' => 'Texas',
  					'UT' => 'Utah',
  					'VT' => 'Vermont',
  					'VA' =>'Virginia',
  					'WA' => 'Washington',
  					'WV' => 'West Virginia',
  					'WI' => 'Wisconsin',
  					'WY' => 'Wyoming'
  				);

	$dropdown = '<select id="selected-state" name="state" class="select">'; 

	foreach ($states_list as $key => $value) {
		if($key == $code){
			$dropdown .= '<option value="'.$key.'" selected >'.$value.'</option>';
		}else{
			$dropdown .= '<option value="'.$key.'" >'.$value.'</option>';
		}
	}

	$dropdown .= '</select>';
	return $dropdown; 
}
 
function bc_woocommerce_save_account_details($user_id) {
 
  update_user_meta( $user_id, 'state', htmlentities( $_POST[ 'state' ] ) );
  update_user_meta( $user_id, 'city', htmlentities( $_POST[ 'city' ] ) );
 
  //$user = wp_update_user( array( 'ID' => $user_id, 'user_url' => esc_url( $_POST[ 'url' ] ) ) );
 
}

/*
*
* Update order status
*
*/
function bc_update_order_status($response){
	global $wpdb;
	$table_name = $wpdb->prefix . 'bc_status';

	$wpdb->update( 
		$table_name, 
		array( 
			'percentage_complete' => $response->percentage_complete,	
			'status' => $response->status
		), 
		array( 
			'order_id' => $response->id 
		)  
	);
}

/*
*
* Activate badge for user
*
*/ 
function bc_is_user_approved($user_id){
	global $wpdb;
	$table_name = $wpdb->prefix . 'bc_status';
	$query = "SELECT * FROM ".$table_name." WHERE user_id = ".$user_id; 
	$result = $wpdb->get_row($query);
	
	if($result->status == "PASS"){
		echo '<img src="" class="profile-badge">';
	}
	
}

/*
*
* Verify if the user has already requested a backgroun check.
* 
*/
function bc_user_is_checked(){
	global $wpdb;
	$table_name = $wpdb->prefix . 'bc_status';

	$query = "SELECT * FROM ".$table_name." WHERE user_id = ".get_current_user_id(); 
	$result = $wpdb->get_row($query);

	return $result != null ? true : false;
}


