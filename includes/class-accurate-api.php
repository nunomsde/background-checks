<?php
/**
 * The Accurate API core class.
 *
 *
 *
 * @since      1.0.0
 * @package    Background_Checks
 * @subpackage Background_Checks/includes
 * @author     Nuno Duarte <nuno@everydayshuffle.com>
 */ 

class Accurate_Api {

	protected $api_id;
	protected $api_secret;
	protected $endpoint = "https://api.accuratebackground.com/v3/";

	public function __construct($id , $secret){

		$this->api_id = $id;
		$this->api_secret = $secret;
	}

	/**
	*
	*  Create candidate 
	*  @return array
	*/
	public function createCandidate($user){
		$url= $this->endpoint."candidate";

    	$data = array(
    	 'firstName' => $user->user_firstname,
    	 'lastName' => $user->user_lastname,	
         'email' => $user->user_email
        );

    	return $this->postRequest($url, $data);
	}

	/**
	*
	* Create an Order
	* @return array 
	*/
	public function createOrder($candidateId , $region, $city){
		$url= $this->endpoint."order";

	    $data = array(
	    	'candidateId' => $candidateId,
	        'packageType' => "PKG_BASIC",
	        'workflow' => "INTERACTIVE",
	        'jobLocation.country' => 'US',
  			'jobLocation.region' => $region,
    		'jobLocation.city' => $city,
    		'additionalProductTypes' => array(
    			'productType' => 'MOV', 
    			'productType' => 'MVR',
    			'productType' => 'EDU',
    			'productType' => 'NCRIM'
    		)
	    );

	    return $this->postRequest($url, $data);
	}

	/**
	*
	* Execute POST request 
	* @return array
	*/
	private function postRequest($url, $body){
 		$result = wp_remote_post( $url, array('headers' => $this->getHeaders() ,'body' => $body ));
		return json_decode(wp_remote_retrieve_body($result));
	}

	/**
	* 
	* Get headers
	*@return array
	*/
	private function getHeaders(){
		$headers = array(
	    	'Authorization' => 'Basic ' . base64_encode( $this->api_id.':'.$this->api_secret)
	    );
	    return $headers;
	}

}
