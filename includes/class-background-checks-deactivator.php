<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://everydayshuffle.com
 * @since      1.0.0
 *
 * @package    Background_Checks
 * @subpackage Background_Checks/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Background_Checks
 * @subpackage Background_Checks/includes
 * @author     Nuno Duarte <nuno@everydayshuffle.com>
 */
class Background_Checks_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
